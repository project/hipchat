<?php

/**
 * @file
 * Hook implementations for HipChat so we can respond to various system events.
 */

/**
 * Implements hook_menu().
 */
function hipchat_menu() {
  $items = array();
  $items['admin/config/services/hipchat'] = array(
    'title' => 'HipChat settings',
    'description' => 'Settings for the HipChat integration module',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('hipchat_admin_form'),
    'access arguments' => array('administer hipchat'),
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function hipchat_permission() {
  return array(
    'administer hipchat' => array(
      'title' => t('Administer HipChat'),
      'description' => t('Perform administration tasks for HipChat.'),
    ),
  );
}

/**
 * Form for the HipChat admin page.
 */
function hipchat_admin_form() {
  $form['hipchat_token'] = array(
    '#type' => 'textfield',
    '#title' => t('HipChat token'),
    '#description' => t('Get an Admin API token from !hipchat_link', array('!hipchat_link' => l(t('your HipChat admin page'), 'https://www.hipchat.com/group_admin/api'))),
    '#default_value' => variable_get('hipchat_token', NULL),
  );
  $form['hipchat_default_room'] = array(
    '#type' => 'textfield',
    '#title' => t('HipChat default room'),
    '#description' => t('Enter the default room to send notices. Enter the human name, not the room id'),
    '#default_value' => variable_get('hipchat_default_room', NULL),
  );
  $form['hipchat_content_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Only send notifications for these content types'),
    '#default_value' => variable_get('hipchat_content_types'),
    '#options' => node_type_get_names(),
    '#description' => t('All types will be included if none are selected.'),
  );

  return system_settings_form($form);
}

/**
 * A wrapper around the HipChat.php to send messages.
 *
 * @param string $message
 *   The message to send. Keep it relatively brief.
 * @param string $room
 *   Optional room to send it to (defaults to the sitewide value).
 * @param string $site_name
 *   Optional "site name" defaults to the Drupal site name.
 */
function hipchat_send($message, $room = NULL, $site_name = NULL) {
  require_once 'hipchat-php//src/HipChat/HipChat.php';

  // Ensure the site_name is less than 15 characters -- an API limit of HipChat.
  $room = empty($room) ? variable_get('hipchat_default_room', 'Development') : $room;
  $site_name = empty($site_name) ? variable_get('site_name', 'Drupal site') : $site_name;
  $site_name = substr($site_name, 0, 15);
  $token = variable_get("hipchat_token", NULL);
  $hc = new HipChat\HipChat($token);

  // Messages won't send properly without all three of these.
  if (empty($site_name) || empty($room) || empty($message)) {
    watchdog('hipchat', 'Skipping sending a message because site_name, room or message were empty');
    return;
  }

  $data = array('room' => $room, 'message' => $message);
  drupal_alter('hipchat_send_message', $data);

  try {
    $hc->message_room($room, $site_name, $message);
  }
  catch (HipChat\HipChat_Exception $e) {
    $params = array(
      '@room' => $room,
      '@msg' => $e->getMessage(),
    );
    watchdog('hipchat', 'Error sending message to room @room: @msg', $params, WATCHDOG_ERROR);
  }
}

/**
 * Implements hook_node_delete().
 */
function hipchat_node_delete($node) {
  if (!_hipchat_is_enabled_type($node)) {
    // Early return if this is not an enabled type.
    return;
  }
  global $user;
  hipchat_send(t('@user_name deleted "@node_title" of type <em>@node_type</em>. !link', array(
    '@user_name' => $user->name,
    '@node_type' => $node->type,
    '@node_title' => $node->title,
    '!link' => l(t('Link'), 'node/' . $node->nid, array('absolute' => TRUE)),
  )));
}

/**
 * Implements hook_node_insert().
 */
function hipchat_node_insert($node) {
  if (!_hipchat_is_enabled_type($node)) {
    // Early return if this is not an enabled type.
    return;
  }
  global $user;
  hipchat_send(t('@user_name created a new <em>@node_type</em>: "@node_title":<br /> !link', array(
    '@user_name' => $user->name,
    '@node_type' => $node->type,
    '@node_title' => $node->title,
    '!link' => l(t('Link'), 'node/' . $node->nid, array('absolute' => TRUE)),
  )));
}

/**
 * Implements hook_node_update().
 */
function hipchat_node_update($node) {
  if (!_hipchat_is_enabled_type($node)) {
    // Early return if this is not an enabled type.
    return;
  }
  global $user;
  hipchat_send(t('@user_name edited \"@node_title\" of type <em>@node_type</em>. !link', array(
    '@user_name' => $user->name,
    '@node_type' => $node->type,
    '@node_title' => $node->title,
    '!link' => l(t('Link'), 'node/' . $node->nid, array('absolute' => TRUE)),
  )));
}

/**
 * Implements hook_comment_insert().
 */
function hipchat_comment_insert($comment) {
  $node = node_load($comment->nid);
  if (!_hipchat_is_enabled_type($node)) {
    // Early return if this is not an enabled type.
    return;
  }
  global $user;
  hipchat_send(t('@user_name added a new comment to "@node_title":<br /> !link', array(
    '@user_name' => $user->name,
    '@node_title' => $node->title,
    '!link' => l(t('Link'), 'node/' . $node->nid, array('absolute' => TRUE, 'fragment' => 'comment-' . $comment->cid)),
  )));
}

/**
 * Implements hook_comment_update().
 */
function hipchat_comment_update($comment) {
  $node = node_load($comment->nid);
  if (!_hipchat_is_enabled_type($node)) {
    // Early return if this is not an enabled type.
    return;
  }
  global $user;
  hipchat_send(t('@user_name edited their comment to "@node_title". !link', array(
    '@user_name' => $user->name,
    '@node_title' => $node->title,
    '!link' => l(t('Link'), 'node/' . $node->nid, array('absolute' => TRUE, 'fragment' => 'comment-' . $comment->cid)),
  )));
}

/**
 * Implements hook_comment_delete().
 */
function hipchat_comment_delete($comment) {
  $node = node_load($comment->nid);
  if (!_hipchat_is_enabled_type($node)) {
    // Early return if this is not an enabled type.
    return;
  }
  global $user;
  hipchat_send(t('@user_name deleted a comment on "@node_title". !link', array(
    '@user_name' => $user->name,
    '@node_title' => $node->title,
    '!link' => l(t('Link'), 'node/' . $node->nid, array('absolute' => TRUE)),
  )));
}

/**
 * Helper function determines if a Node is of an enabled type.
 *
 * @param object $node 
 *   A Node object with a 'type' property.
 *
 * @return mixed
 *   Returns TRUE if hipchat is enabled for all types.
 */
function _hipchat_is_enabled_type($node) {
  $types = variable_get('hipchat_content_types', array());
  // This filter gets us an empty array if no types have been selected.
  $types = array_filter($types, function ($value) {
    return $value !== 0;
  }
  );
  // If none are selected it's enabled for all types.
  if (empty($types)) {
    return TRUE;
  }
  else {
    return array_key_exists($node->type, $types);
  }
}
